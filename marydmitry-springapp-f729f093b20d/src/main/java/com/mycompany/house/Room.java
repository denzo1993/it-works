
package com.mycompany.house;

public interface Room {
    
    public void afterConstructor();
    public void increaseTemp(double value);
    public void reduceTemp(double value);
    public void setNaturalLighting(double value);
    public double getNaturalLighting();
    public void roomStateChanged();
    public Log getLog();
    public void setHeating(Heating heating);
    public Heating getHeating();
    public Window getWindow();
    public void setWindow(Window window);
    public Door getDoor();
    public void setDoor(Door door);
    public double getMin_temp();
    public void setMin_temp(double min_temp);
    public double getMax_temp();
    public void setMax_temp(double max_temp);
    public int getUpdateInterval();
    public Controller getController();
    public void setController(Controller controller);
    public Log getL();
    public void setL(Log l);
    public void setTemp(double value);
    public double getTemp();
    public double getMin_lighting();
    public void setMin_lighting(double min_lighting);
    public double getNorm_lighting();
    public void setNorm_lighting(double norm_lighting);
    public Lamp getLamp();
    public void setLamp(Lamp lamp);
    
}