package com.mycompany.house;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        
        ApplicationContext context = new ClassPathXmlApplicationContext("Config.xml");
        
//	House house = (House) context.getBean("House");
//        System.out.println("Room count in the house = " + house.RoomCount());
//        
//        Room bedroom = (Room) context.getBean("FirstBedroom");
//        bedroom.allconnect();
//        System.out.println("\nInfo about First Bedroom: ");
//        bedroom.Info();
//        
//        //запуск потока с изменением состояния комнаты
//        Changing bedroomC = new Changing(bedroom);
//        Thread bedroomT = new Thread(bedroomC);
//        bedroomT.start();
//
//        
//        Room bedroom2 = (Room) context.getBean("SecondBedroom");
//        bedroom2.allconnect();
//        System.out.println("\nInfo about Second Bedroom: ");
//        bedroom2.Info();
//
//        Changing bedroom2C = new Changing(bedroom2);
//        Thread bedroom2T = new Thread(bedroom2C);
//        bedroom2T.start();
    }
}
