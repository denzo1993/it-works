/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.main;

import com.mycompany.house.Room;
import java.net.URL;
import java.util.Enumeration;
import java.util.Set;
import org.reflections.Reflections;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

/**
 *
 * @author Volt
 */
public class RoomBuilder {
    public Room build() throws Exception {
        final ClassLoader contextClassLoader = Thread.currentThread().
                getContextClassLoader();
        System.out.println("contextClassLoader" + contextClassLoader.getResource("module.txt"));
        Enumeration<URL> resources = contextClassLoader.getResources("module.txt");
        //InputStream resourceAsStream = contextClassLoader.getResourceAsStream("moduleDescription.txt.txt");
        while (resources.hasMoreElements()) {
            URL url = resources.nextElement();
            System.out.println("url=" + url);
        }

        Reflections reflections = new Reflections(
                new ConfigurationBuilder()
                .setUrls(ClasspathHelper.forClassLoader(contextClassLoader))
        );
        Set<Class<? extends Room>> types = reflections.getSubTypesOf(Room.class);
        System.out.println("types=" + types);
        for (Class<? extends Room> type : types) {

            Room newInstance = (Room) contextClassLoader.loadClass(type.getName()).getConstructor().newInstance();
            System.out.println("newInstance=" + newInstance.toString());
            return newInstance;
        }
        return null;

    }
}
