/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.room;

import com.mycompany.house.Controller;
import com.mycompany.house.Door;
import com.mycompany.house.Heating;
import com.mycompany.house.Lamp;
import com.mycompany.house.Room;
import com.mycompany.house.Window;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import com.mycompany.house.Log;

/**
 *
 * @author Денис
 */
public class MyRoom implements Room{
    
    private static final int updateInterval = 1000;
    private double min_temp;
    private double max_temp;
    private double temp; // Текущая темпа
    
    private double min_lighting;
    private double norm_lighting;
    private double naturalLighting;

    private Timer lightingTimer;
    
    private Controller controller;

    private Lamp lamp;

    private Heating heating;
    private Door door;
    private Window window;
    private Log l;
    
    /**
     * В xml указано, что этот метод будет вызываться после создания объекта Room
     */
    @Override
    public void afterConstructor() {
        l.log("Комната создана");
        roomStateChanged();
        lightingTimer = new Timer();
        lightingTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                double value = (double) (new Random().nextInt()%5) / 100;
                naturalLighting += value;
                roomStateChanged();
            }
        }, 0, updateInterval);
    }
    
    /**
     * Метод увеличения темпы
     * @param value 
     */
    @Override
    public void increaseTemp(double value) {
        temp += value;
        l.log("Температура увеличена на " + value);
        roomStateChanged();
    }
    
    /**
     * Метод уменьшения темпы
     * @param value 
     */
    @Override
    public void reduceTemp(double value) {
        temp -= value;
        l.log("Температура уменьшена на " + value);
        roomStateChanged();
    }
    
    @Override
    public void setNaturalLighting(double value) {
        naturalLighting = value;
    }
    
    @Override
    public double getNaturalLighting() {
        return naturalLighting;
    }
    
    @Override
    public void roomStateChanged() {
        controller.stateChangedHandler();
    }
    
    @Override
    public Log getLog() {
        return l;
    }
    
    @Override
    public void setHeating(Heating heating) {
        this.heating = heating;
    }
    
    @Override
    public Heating getHeating() {
        return heating;
    }
    
    @Override
    public Window getWindow() {
        return window;
    }
    
    @Override
    public void setWindow(Window window) {
        this.window = window;
    }
    
    @Override
    public Door getDoor() {
        return door;
    }
    
    @Override
    public void setDoor(Door door) {
        this.door = door;
    }
    
    @Override
    public double getMin_temp() {
        return min_temp;
    }

    @Override
    public void setMin_temp(double min_temp) {
        this.min_temp = min_temp;
    }

    @Override
    public double getMax_temp() {
        return max_temp;
    }

    @Override
    public void setMax_temp(double max_temp) {
        this.max_temp = max_temp;
    }
    
    @Override
    public int getUpdateInterval() {
        return updateInterval;
    }
    
    @Override
    public Controller getController() {
        return controller;
    }

    @Override
    public void setController(Controller controller) {
        this.controller = controller;
    }
    
    @Override
    public Log getL() {
        return l;
    }

    @Override
    public void setL(Log l) {
        this.l = l;
    }
    
    @Override
    public void setTemp(double value) {
        temp = value;
    }
    
    @Override
    public double getTemp() {
        return temp;
    }
    
    @Override
    public double getMin_lighting() {
        return min_lighting;
    }

    @Override
    public void setMin_lighting(double min_lighting) {
        this.min_lighting = min_lighting;
    }

    @Override
    public double getNorm_lighting() {
        return norm_lighting;
    }

    @Override
    public void setNorm_lighting(double norm_lighting) {
        this.norm_lighting = norm_lighting;
    }
    
    @Override
    public Lamp getLamp() {
        return lamp;
    }

    @Override
    public void setLamp(Lamp lamp) {
        this.lamp = lamp;
    }

}